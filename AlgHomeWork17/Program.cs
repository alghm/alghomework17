﻿
var edges = new List<Edge>
{
   new Edge(0, 1),
   new Edge(1, 2),
   new Edge(2, 3),
   new Edge(3, 4),
   new Edge(4, 0),
   new Edge(2, 4),
   new Edge(3, 1),
};

Console.WriteLine("Изначальные рёбра графа: ");
foreach (var edge in edges)
{
    Console.WriteLine($"{edge.v1} - {edge.v2}");
}

var graph = new Graph(edges);
var result = graph.Kruskal();


Console.WriteLine("\nРёбра в минимальном остовном дереве:");
foreach (var edge in result)
{
    Console.WriteLine($"{edge.v1} - {edge.v2}");
}

Console.WriteLine("\n\n");
 

public class Edge
{
    public int v1;
    public int v2;

    public Edge(int v1, int v2)
    {
        this.v1 = v1;
        this.v2 = v2;
    }
}

public class UnionFind
{
    private int[] parent;
    private int[] rank;

    public UnionFind(int n)
    {
        parent = new int[n];
        rank = new int[n];

        for (int i = 0; i < n; i++)
        {
            parent[i] = i;
            rank[i] = 0;
        }
    }

    public int Find(int v)
    {
        if (v == parent[v])
        {
            return v;
        }

        return parent[v] = Find(parent[v]);
    }

    public void Union(int v1, int v2)
    {
        int r1 = Find(v1);
        int r2 = Find(v2);

        if (r1 != r2)
        {
            if (rank[r1] > rank[r2])
            {
                parent[r2] = r1;
            }
            else if (rank[r1] < rank[r2])
            {
                parent[r1] = r2;
            }
            else
            {
                parent[r2] = r1;
                rank[r1]++;
            }
        }
    }
}

public class Graph
{
    private List<Edge> edges;

    public Graph(List<Edge> edges)
    {
        this.edges = edges;
    }

    public List<Edge> Kruskal()
    {
        var res = new List<Edge>();
        var uf = new UnionFind(edges.Max(e => Math.Max(e.v1, e.v2)) + 1);

        foreach (var edge in edges)
        {
            int r1 = uf.Find(edge.v1);
            int r2 = uf.Find(edge.v2);

            if (r1 != r2)
            {
                res.Add(edge);
                uf.Union(r1, r2);
            }
        }

        return res;
    }
}
